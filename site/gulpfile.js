var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');

var paths = {
  sass: ['./public/scss/base.scss'],
  dest: "./public/css/"
};

gulp.task('default', ['sass']);

gulp.task('sass', function(done) {
  gulp.src(paths.sass)
      .pipe(sass())
      .on('error', sass.logError)
      .pipe(gulp.dest(paths.dest))
      .pipe(minifyCss({
        keepSpecialComments: 0
      }))
      .pipe(rename({ extname: '.min.css' }))
      .pipe(gulp.dest(paths.dest))
      .on('end', done);
});

gulp.task('watch', function() {
  gulp.watch("./public/scss/**/*.scss", ['sass']);
});