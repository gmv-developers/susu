var express = require('express');
var app = express();
var server = require('http').Server(app);
var cors = require('cors');
var multer  = require('multer');
var port = process.env.PORT || 8081;
var ip = require("./api/ip");
var nodemailer = require('nodemailer');

app.use(cors());

app.use(express.static('public'));
app.use(function(req, res) {
  res.sendFile(__dirname + '/public/index.html');
});

server.listen(port, function(){
  ip.getNetworkIPs(function(error, ip){
    console.log("Server started at port " + ip + ":" + port);
  });
});




