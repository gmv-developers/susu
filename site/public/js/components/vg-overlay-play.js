/**
 * @license videogular v1.4.4 http://videogular.com
 * Two Fucking Developers http://twofuckingdevelopers.com
 * License: MIT
 */
/**
 * @ngdoc directive
 * @name com.2fdevs.videogular.plugins.overlayplay.directive:vgOverlayPlay
 * @restrict E
 * @description
 * Shows a big play button centered when player is paused or stopped.
 *
 * <pre>
 * <videogular vg-theme="config.theme.url" vg-autoplay="config.autoPlay">
 *    <vg-media vg-src="sources"></vg-media>
 *
 *    <vg-overlay-play></vg-overlay-play>
 * </videogular>
 * </pre>
 *
 */
"use strict";
angular.module("com.2fdevs.videogular.plugins.overlayplay", [])
    .run(
        ["$templateCache", function ($templateCache) {
            $templateCache.put("vg-templates/vg-overlay-play",
                '<div class="overlayPlayContainer" ng-click="onClickOverlayPlay()">\
                    <div class="playbutton">\
                        <a class="playBut">\
                            <div class="control {{isPlaying}}" layout="row" layout-align="center center" >\
                                <span class="left"></span><span class="right"></span>\
                            </div>\
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"\
                            x="0px" y="0px" width="95px" height="95px" viewBox="0 0 213.7 213.7" enable-background="new 0 0 213.7 213.7">\
                                <circle class="circle" id="XMLID_17_" fill="none"  stroke-width="3" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="106.8" cy="106.8" r="103.3"/>\
                            </svg>\
                        </a>\
                    </div>\
                </div>');
        }]
    )
    .directive("vgOverlayPlay", ["VG_STATES",
        function (VG_STATES) {
            return {
                restrict: "E",
                require: "^videogular",
                scope: {},
                templateUrl: function (elem, attrs) {
                    return attrs.vgTemplate || 'vg-templates/vg-overlay-play';
                },
                link: function (scope, elem, attr, API) {
                    scope.isPlaying = "play";
                    scope.onChangeState = function onChangeState(newState) {
                        switch (newState) {
                            case VG_STATES.PLAY:
                                scope.overlayPlayIcon = {};
                                scope.isPlaying = "pause";
                                break;

                            case VG_STATES.PAUSE:
                                scope.overlayPlayIcon = {play: true};
                                scope.isPlaying = "play";
                                break;

                            case VG_STATES.STOP:
                                scope.overlayPlayIcon = {play: true};
                                scope.isPlaying = "play";
                                break;
                        }
                    };
                    scope.onClickOverlayPlay = function onClickOverlayPlay(event) {
                        API.playPause();
                    };
                    scope.overlayPlayIcon = {play: true};

                    scope.$watch(
                        function () {
                            return API.currentState;
                        },
                        function (newVal, oldVal) {
                            scope.onChangeState(newVal);
                        }
                    );
                }
            }
        }
    ]);

