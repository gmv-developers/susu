(function () {
    angular
        .module("app")
        .factory('dialogService',
            [
                '$mdMedia',
                '$mdDialog',
                '$sce',
                dialogServices
            ]
        );

    function dialogServices($mdMedia,$mdDialog, $sce) {
        var dialogServices = {};

        //Popup dialog
        function showDialog(opt) {
            return $mdDialog.show(opt);
        }


        //
        dialogServices.showMediaDetailsDialog = function (ev, videoUrl) {
            console.log(videoUrl);
            return showDialog({
                controller: ['$scope', function ($scope) {
                    $scope.config = {
                        preload: "none",
                        sources: [{src: $sce.trustAsResourceUrl(videoUrl), type: "video/mp4"}]
                    };
                }],
                templateUrl: 'views/dialogPreview.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true
            });
        };
        return dialogServices;
    }
})();