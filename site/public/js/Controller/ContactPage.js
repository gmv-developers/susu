app.controller("ContactCtrl", function($scope) {

    $scope.fields=[
        {
            "id":"fName",
            "hint":"Name_",
            "size":"normal",
            "maxlen": "20"
        },
        {
            "id":"fEmail",
            "hint":"Email_",
            "size":"normal",
            "maxlen": "50"
        },
        {
            "id":"fPhone",
            "hint":"Phone_",
            "size":"normal",
            "maxlen": "15"
        },
        {
            "id":"fCompany",
            "hint":"Company_",
            "size":"normal",
            "maxlen": "50"
        },
        {
            "id":"fMessage",
            "hint":"Message_",
            "size":"larger"
        }
    ]

    $scope.sendMail = function(){
        var smtpTransport = nodemailer.createTransport("SMTP",{
            service: "Gmail",
            auth: {
                user: "netchserver@gmail.com",
                pass: "netchserver123"
            }
        });
        smtpTransport.sendMail({
            from: "SuSu HomePage <netchserver@gmail.com>", // sender address.  Must be the same as authenticated user if using Gmail.
            to: "Client <huenghidev@email.com>", // receiver
            subject: "Buy app", // subject
            text: "Test" // body
        },function(error, response){  //callback
            if(error){
                console.log(error);
            }else{
                console.log("Message sent: " + response.message);
            }

            smtpTransport.close(); // shut down the connection pool, no more messages.  Comment this line out to continue sending emails.
        });
    }
});