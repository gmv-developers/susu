app.controller("previewCtr",function ($scope, $sce,videoUrl) {
        $scope.config = {
            preload: "none",
            sources: [
                {src: $sce.trustAsResourceUrl(videoUrl), type: "video/mp4"},
                {src: $sce.trustAsResourceUrl(videoUrl), type: "video/webm"},
                {src: $sce.trustAsResourceUrl(videoUrl), type: "video/ogg"}
            ],
            tracks: [
                {
                    src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
                    kind: "subtitles",
                    srclang: "en",
                    label: "English",
                    default: ""
                }
            ],
            theme: {
                url: "http://www.videogular.com/styles/themes/default/latest/videogular.css"
            }
        }
    }
);