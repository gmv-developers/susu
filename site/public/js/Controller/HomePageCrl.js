(function(){
    angular.module("app").controller("HomepageCtrl", HomepageCtrl);

    HomepageCtrl.$inject = ["$sce","$scope", '$window'];

    function HomepageCtrl($sce,$scope, $window) {

        //Config video
        this.onPlayerReady = function(API) {
            this.API = API;
        };
        this.config = {
            sources: [
                {src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.mp4"), type: "video/mp4"},
                {src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.webm"), type: "video/webm"},
                {src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.ogg"), type: "video/ogg"}
            ],
            tracks: [
                {
                    src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
                    kind: "subtitles",
                    srclang: "en",
                    label: "English",
                    default: ""
                }
            ],
            theme: {
                url: "../lib/videogular-themes-default/videogular.css"
            },
            plugins: {
                poster: "../images/home_page_poster.png"
            }
        };

        //Resize video
        $scope.$on("$viewContentLoaded", function(){
            resizeVideo();
        })
        $(window).resize(function(){
            resizeVideo();
        })
        function resizeVideo(){
            var maxHeight = $window.innerHeight - $("#header").height() - $("#footer").height();
            var expectedHeight = ($window.innerWidth*80/100)/1140*550;
            if((maxHeight-expectedHeight)/2 < 50){
                $("#video").css("height", maxHeight-100);
            }
            else{
                $("#video").css("height", expectedHeight);
            }
        }

        //Set play/pause effect
        $scope.onMobile = $window.navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/) != null;
        this.playPause = function(id){
            if((id=="overlay" && $scope.onMobile) || (id!="overlay" && !$scope.onMobile)) {
                $("#play").toggleClass("play_btn_show play_btn_hide");
                $("#pause").toggleClass("play_btn_show play_btn_hide");
                this.API.playPause();
            }
        };
        $("#play_circle").bind('oanimationend animationend webkitAnimationEnd', function() {

            var playCircle = document.getElementById("play_circle");

            if(playCircle.classList.contains('circle_backward')){
                playCircle.style.strokeDashoffset = '295.16';//r:47
            }
            else{
                playCircle.style.strokeDashoffset = '0';
            }
        });
        $("#play_control").bind('oanimationend animationend webkitAnimationEnd', function() {

            var playControl = document.getElementById("play_control");

            if(playControl.classList.contains('play_btn_fadeOut')){
                playControl.style.opacity = '0';
            }
            else{
                playControl.style.opacity = '1';
            }
        });
        $("#overlay").mouseenter(function() {
            $("#play_control").removeClass("play_btn_fadeOut").addClass("play_btn_fadeIn");
        });
        $("#overlay").mouseleave(function() {
            $("#play_control").removeClass("play_btn_fadeIn").addClass("play_btn_fadeOut");
        });
        $("#playButton").mouseenter(function() {
            $("#play_circle").removeClass("circle_backward").addClass("circle_forward");
        });
        $("#playButton").mouseleave(function() {
            $("#play_circle").removeClass("circle_forward").addClass("circle_backward");
        });
    }
})();
