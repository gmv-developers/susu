app.controller("ThemeCtrl", ['$scope','$rootScope','$route','$timeout','THEMES','dialogService',
    function ($scope, $rootScope, $route,$timeout, THEMES,dialogService) {

        //Adjust position of all theme button
        function adjustAllThemePosition(){
            var top = $(".content").position().top + $(".content").height();
            if($(".block.collapse:hidden").length > 0){
                top = top + $("#all_theme").height();
            }
            $("#all_theme").css({top: top});
        }
        $timeout(function (){
            adjustAllThemePosition();
        },100);
        $(window).resize(function () {
            adjustAllThemePosition();
        });


        //Get themes
        $scope.themes = [[[],[]],[[],[]]];
        for(var i=0; i < THEMES.length; i++){
            if(i%2==0){
                $scope.themes[0][0].push(THEMES[i]);
                if(i%4==2){
                    $scope.themes[1][0].push(THEMES[i]);
                }
            }
            else {
                $scope.themes[0][1].push(THEMES[i]);
                if(i%4==3){
                    $scope.themes[1][1].push(THEMES[i]);
                }
            }
        }


        //Scroll event
        $scope.scrollTop   = -1;
        $scope.scrollUp    = 0;
        $scope.scrollDown  = 0;
        $scope.curIndex    = 0;
        $scope.allowScroll = true;

        $scope.onMouseWheelUp = function (event) {
            if($(".content").scrollTop()==$scope.scrollTop) {
                $scope.scrollUp++;
                if($scope.allowScroll && $scope.scrollUp%30==0) scroll("down");
            }
            else{
                $scope.scrollTop = $(".content").scrollTop();
            }
        };
        $scope.onMouseWheelDown = function (event) {
            if($(".content").scrollTop()==$scope.scrollTop) {
                $scope.scrollDown++;
                if($scope.allowScroll && $scope.scrollDown%30==0) scroll("up");
            }
            else{
                $scope.scrollTop = $(".content").scrollTop();
            }
        };
        function scroll(direction) {

            var nextIndex = direction=="up" ? ($scope.curIndex+1) : ($scope.curIndex-1);
            var numItemInBlock = $($(".block")[0]).find(".item:visible").length;

            if(nextIndex >= 0 && nextIndex < numItemInBlock){

                //inactive scroll
                $scope.allowScroll = false;

                //inactive overlay
                $(".item").find(".overlay").hide();


                //set duration & delay
                var duration = 0.4;
                var delay = duration/2;

                //moving current items
                var curItems  = $(".block").find(".item:visible:eq(" + $scope.curIndex + ")");
                for(var i = 0; i < curItems.length; i++){
                    var cDelay = (i==1 || i==2) ? delay : 0;
                    var moveDist = direction=="up" ? "-=140%" : "+=100%";
                    TweenLite.to(curItems[i], duration, {
                        y: moveDist,
                        delay: cDelay
                    });
                }

                //moving next items
                var nextItems = $(".block").find(".item:visible:eq(" + nextIndex + ")");
                for(var i = 0; i < nextItems.length; i++){
                    var moveDist = direction=="up" ? "-=100%" : "+=140%";
                    var cDelay = delay + ((i==1 || i==2) ? delay : 0);
                    TweenLite.to(nextItems[i], duration, {
                        y: moveDist,
                        delay: cDelay,
                        onComplete: finishScroll,
                        onCompleteParams: [i==1]
                    });
                }
                function finishScroll(lastItem) {
                    if (lastItem) {
                        $(".item").find(".overlay").show(); //active overlay back
                        $scope.allowScroll = true; //active scroll;
                    }
                }

                //update current index
                $scope.curIndex = nextIndex;
            }
            else{
                $scope.scrollUp = $scope.scrollDown = 0;
            }



            /*if ($scope.nItemInBlock == 0) setNumItemInBlock();

            //allow to scroll
            if ((direction == "up" && ($scope.curItem + $scope.nItemInBlock) < $scope.themes.length)
                || (direction == "down" && ($scope.curItem - $scope.nItemInBlock) >= 0)) {

                //inactive scroll
                $scope.allowScroll = false;

                //inactive overlay
                $(items).find(".block_item_overlay").hide();

                //determine distance to move
                var moveDist = $("#theme4_content").height();

                for (var iItem = 0; iItem < items.length; iItem++) {

                    var target = items[iItem];
                    var delay = 0;
                    var targetY = (direction == "up" ? "-=" : "+=" ) + moveDist;
                    var isLast = (direction == "up" ? (iItem == items.length - 1) : (iItem == 0));


                    //1. delay in block
                    if (($scope.nItemInBlock == 4 && (iItem % 4 == 1 || iItem % 4 == 2)) || ($scope.nItemInBlock == 2 && iItem % 2 == 1)) {
                        delay = 0.1;
                    }
                    //2. delay among blocks
                    if ((direction == "up" && iItem >= ($scope.curItem + $scope.nItemInBlock)) //scroll up and items below current block
                        || (direction == "down" && iItem < $scope.curItem)) { //or: scroll down and items upper current block
                        delay += 0.3;
                    }

                    //3. moving
                    TweenLite.to(target, 0.6, {
                        top: targetY,
                        delay: delay,
                        onComplete: finishScroll,
                        onCompleteParams: [isLast]
                    });
                    function finishScroll(lastItem) {
                        if (lastItem) {
                            $(items).find(".block_item_overlay").show(); //active overlay back
                            $scope.allowScroll = true; //active scroll;
                        }
                    }
                }
                //update current item after scroll
                $scope.curItem += (direction == "up") ? $scope.nItemInBlock : -$scope.nItemInBlock;
            }
            else {
                if (direction == "down") $scope.scrollUp = 0;
                else if (direction == "up") $scope.scrollDown = 0;
            }*/
        }


        //Show preview
        $scope.showPreview = dialogService.showMediaDetailsDialog;
    }]);

app.controller("alltheme", ['$scope', 'THEMES', 'dialogService', function ($scope, THEMES, dialogService) {
    var Perrow = [];
    $scope.theme = [];
    $scope.alltheme = [];
    $scope.alltheme = THEMES;
    console.log($scope.alltheme);
    for (i = 0; i < $scope.alltheme.length / 3; i++) {
        for (j = 0; j < $scope.alltheme.length - i * 3 && j < 3; j++) {
            Perrow.push({
                url: $scope.alltheme[j + 3 * i].url,
                name: $scope.alltheme[j + 3 * i].name,
                videoUrl: $scope.alltheme[j + 3 * i].videoUrl
            })
        }
        $scope.theme.push(Perrow);
        Perrow = [];
    }

    $scope.showPreview = dialogService.showMediaDetailsDialog;
}]);