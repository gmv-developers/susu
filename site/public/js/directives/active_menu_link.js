angular.module('activeLink', [])
    .directive('activeLink', ['$location', function (location) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs, controller) {
            var clazz = "active-link";
            var path = attrs.href;
            scope.location = location;
            scope.$watch('location.path()', function (newPath) {
                if(newPath =="/alltheme"){
                    newPath = "/theme";
                }
                if (path === newPath) {
                    element.addClass(clazz);
                    element.removeClass("donthover");
                }
                else {
                    element.addClass("donthover");
                    element.removeClass(clazz);
                }
            });
        }
    };
}]);
