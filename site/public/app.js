'use strict';
var app = angular.module("app", ["ngRoute",
    "ngSanitize",
    "ngMaterial",
    "ngMessages",
    "material.svgAssetsCache",
    "com.2fdevs.videogular",
    "com.2fdevs.videogular.plugins.controls",
    "com.2fdevs.videogular.plugins.overlayplay",
    "com.2fdevs.videogular.plugins.poster",
    "alAngularHero",
    "MyHamburgerBtn",
    "activeLink",
    "ngMouseWheel"
]);
app.run(["$rootScope", function ($rootScope) {
    $rootScope.selected = 0;
}]);
app.config(function ($routeProvider, $locationProvider) {
  $routeProvider
      .when("/", {
        templateUrl: "views/homepage.html",
        controller: "HomepageCtrl as controller"
      })
      .when("/theme", {
        templateUrl: "views/theme.html",
        controller: "ThemeCtrl"
      })
      .when("/alltheme", {
          templateUrl: "views/all-theme.html",
          controller: "alltheme"
      })
      .when("/document", {
        templateUrl: "views/document.html",
        controller: "DocumentCtrl"
      })
      .when("/contact", {
        templateUrl: "views/contact.html",
        controller: "ContactCtrl"
      });

  $routeProvider.otherwise("/");

  $locationProvider.html5Mode(true);
});
app.constant("THEMES",[
        {
            url: "../images/themes/theme1.jpg",
            name: "THEME1",
            videoUrl: "http://static.videogular.com/assets/videos/videogular.mp4"
        },
        {
            url: "../images/themes/theme2.jpg",
            name: "THEME2",
            videoUrl: "http://static.videogular.com/assets/videos/videogular.mp4"
        },
        {
            url: "../images/themes/theme3.jpg",
            name: "THEME3",
            videoUrl: "http://static.videogular.com/assets/videos/videogular.mp4"
        },
        {
            url: "../images/themes/theme4.jpg",
            name: "THEME4",
            videoUrl: "http://static.videogular.com/assets/videos/videogular.mp4"
        }/*,
        {
            url: "../images/themes/theme4.jpg",
            name: "THEME5",
            videoUrl: "http://static.videogular.com/assets/videos/videogular.mp4"
        },
        {
            url: "../images/themes/theme3.jpg",
            name: "THEME6",
            videoUrl: "http://static.videogular.com/assets/videos/videogular.mp4"
        },
        {
            url: "../images/themes/theme2.jpg",
            name: "THEME7",
            videoUrl: "http://static.videogular.com/assets/videos/videogular.mp4"
        },
        {
            url: "../images/themes/theme1.jpg",
            name: "THEME8",
            videoUrl: "http://static.videogular.com/assets/videos/videogular.mp4"
        },
        {
            url: "../images/themes/theme1.jpg",
            name: "THEME9",
            videoUrl: "http://static.videogular.com/assets/videos/videogular.mp4"
        },
        {
            url: "../images/themes/theme1.jpg",
            name: "THEME10",
            videoUrl: "http://static.videogular.com/assets/videos/videogular.mp4"
        },
        {
            url: "../images/themes/theme1.jpg",
            name: "THEME11",
            videoUrl: "http://static.videogular.com/assets/videos/videogular.mp4"
        },
        {
            url: "../images/themes/theme1.jpg",
            name: "THEME12",
            videoUrl: "http://static.videogular.com/assets/videos/videogular.mp4"
        }*/
]);
