var gulp = require('gulp');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var paths = {
    javascript: ['./public/js/directive/js/gm-animation.js'],
    dest: "./public/js/directive/js/min"
};

gulp.task('build', function() {
    return gulp.src(paths.javascript)
        .pipe(uglify())
        .pipe(gulp.dest(paths.dest))
        .pipe(rename({
            basename: "animation",
            extname: '.min.js'}))
        .pipe(gulp.dest(paths.dest))
        .on('end', function () {
            console.info("build finished")
        });
});
