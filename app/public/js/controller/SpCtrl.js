app.controller("SpCtrl",function($scope, appFactory, socket, $timeout, $mdDialog){
  $scope.enableDrag = false;
  var dialog = null;
  var res = null;
  var orientation = 0;
  $scope.file = [];
  checkscreen();

  function checkscreen(){$timeout(function(){
    if(window.innerWidth > window.innerHeight){
      $scope.camera = "camera2";
      $scope.handcss = "handcss2"
    }else{
      $scope.camera = "camera1";
      $scope.handcss = "handcss1"
    }
    checkscreen();
  },300)}

  document.addEventListener("change", function(evt){
    $scope.determinateValue = 0;
    $mdDialog.show({
      templateUrl: 'loading.html',
      parent: angular.element(document.body),
      clickOutsideToClose:false,
      bindToController: true,
      controller:function($scope){
        $scope.determinateValue = 0;
        dialog = $scope;
      }
    });
    var file  = evt.target.files[0];
    EXIF.getData(file, function() {
      orientation = this.exifdata.Orientation;
    });
    var reader = new FileReader();
    reader.onload = function(event) {
      var imageSource = new Image();
      var imageTarget = new Image();

      imageTarget.onload = function () {
        data = {
          'orientation' : orientation
        };
        
        jic.upload(imageTarget, appFactory.url + "/api/photo", 'file', orientation + ".jpg",
            function(result){
              $timeout(function(){
                res = JSON.parse(result);
                $scope.file = [res.path];
                $mdDialog.hide();
                $scope.enableDrag = true;
              }, 0);
        });
      };

      imageSource.onload = function(){
        imageTarget.src = jic.compress(imageSource, 50, "jpg").src;
      };

      imageSource.src = event.target.result;
    };

    reader.readAsDataURL(file);
  }, false);


  $scope.$on("thumb-release", function(){
    socket.emit('photo-to-server', res);
    $timeout(function(){
      $scope.enableDrag = false;
      $scope.file = [];
    },0);
  })
});

/*

resize.init();
resize.photo(fileItem._file,1080,'file',function (resizeFile) {
    fileItem._file = resizeFile;
});*/
