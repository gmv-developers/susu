app.controller("PcCtrl", function($scope, socket, $timeout, appFactory){
  var isFirst = false;
  $scope.imageFirst = {
    src: "",
    className:"",
    orientation:""
  };

  $scope.imageSecond = {
    src: "",
    className:"",
    orientation:""
  };

  socket.on("fetch-data", function(data){
    if(data){
      $timeout(function(){
        $scope.imageFirst.src = data.src;
        $scope.imageFirst.orientation = data.orientation;
        $scope.imageSecond.src = data.src;
        $scope.imageFirst.className = "active";
        isFirst = true;
      }, 0);
    }
  });
  socket.on("photo-to-client", function(data){
    $timeout(function(){
      if(isFirst){
        $scope.imageSecond.src = data.src;
        $scope.imageSecond.orientation = data.orientation;
        $scope.imageSecond.className = "active";
        $scope.imageFirst.className = "";
      }else{
        $scope.imageFirst.src = data.src;
        $scope.imageFirst.orientation = data.orientation;
        $scope.imageFirst.className = "active";
        $scope.imageSecond.className = ""
      }
      isFirst = !isFirst;
    }, 0);
  });

  angular.element(document.body).dblclick(function(){
    appFactory.toggleFullScreen();
  });
});

