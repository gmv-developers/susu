app.controller("QaCodeCtrl", function($scope, appFactory){
  new QRCode("qrCode", {
    text: appFactory.url + "/sp",
    width: 300,
    height: 300
  });
});