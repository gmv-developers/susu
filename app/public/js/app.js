var app = angular.module("app", ["ngRoute", "ngMaterial", "ngAnimate", "ngResource"]);
app.config(function ($routeProvider, $locationProvider) {
  $routeProvider
      .when("/", {
        templateUrl: "js/views/qa-code.html",
        controller: "QaCodeCtrl"
      })
      .when("/sp", {
        templateUrl: "js/views/sp.html",
        controller: "SpCtrl"
      })
      .when("/pc", {
        templateUrl: "js/views/pc.html",
        controller: "PcCtrl"
      })
      .when("/slide-show", {
        templateUrl: "js/views/slide-show.html",
        controller: "SlideShowCtrl"
      });

  $routeProvider.otherwise("/pc");

  $locationProvider.html5Mode(true);
});

app.factory("appFactory", function ($location) {
  return {
    url: $location.protocol() + "://" + $location.host() + ":" + $location.port(),
    toggleFullScreen: function() {
    if ((document.fullScreenElement && document.fullScreenElement !== null) ||
        (!document.mozFullScreen && !document.webkitIsFullScreen)) {
      if (document.documentElement.requestFullScreen) {
        document.documentElement.requestFullScreen();
      } else if (document.documentElement.mozRequestFullScreen) {
        document.documentElement.mozRequestFullScreen();
      } else if (document.documentElement.webkitRequestFullScreen) {
        document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
      }
    } else {
      if (document.cancelFullScreen) {
        document.cancelFullScreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.webkitCancelFullScreen) {
        document.webkitCancelFullScreen();
      }
    }
  }
  }
});

app.factory("socket", function (appFactory) {
  return io.connect(appFactory.url);
});

app.factory("api", function($resource){
  return {
    photo: function(){
      return $resource("/api/photo");
    }
  }
});

app.directive('ngThumb', ['$window', function ($window) {
  // var helper = {
  //   support: !!($window.FileReader && $window.CanvasRenderingContext2D),
  //   isFile: function (item) {
  //     return angular.isObject(item) && item instanceof $window.File;
  //   },
  //   isImage: function (file) {
  //     var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
  //     return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
  //   }
  // };

  return {
    restrict: 'A',
    template: '<canvas id=\"canvas\"/>',
    link: function (scope, element, attributes) {


      var params = scope.$eval(attributes.ngThumb);

      // if (!helper.support) return;
      // if (!helper.isFile(params.file)) return;
      // if (!helper.isImage(params.file)) return;

      var canvas = element.find('canvas');
      var img = new Image();
      var ctx = canvas[0].getContext('2d');
      var canvasOffset = canvas.offset();
      var offsetX = 0;
      var offsetY = 0;
      var canvasWidth = 0;
      var canvasHeight = 0;
      var isDragging = false;
      var s = 0;
      var pointImage = {x:0, y:0};
      var pointStart = {x:0, y:0};
      var diffDistance = {x:0, y:0};
      var pointImageOriginal = {x: 0, y: 0};
      var releaseThumb = false;
      img.onload = onLoadImage;
      img.src = params.file;
      console.log(params);

      function onLoadImage() {
        canvasWidth = $window.innerWidth;
        canvasHeight = $window.innerHeight;
        s = $window.innerWidth > $window.innerHeight ? $window.innerHeight : $window.innerWidth;
        var k = img.width/img.height;
        canvas.attr({width: canvasWidth, height: canvasHeight});
        canvasOffset = canvas.offset();
        offsetX = canvasOffset.left;
        offsetY = canvasOffset.top;
        if(k<1){
          if(canvasWidth < canvasHeight){
            pointImage.x = canvasWidth/9.2;
            pointImage.y = canvasHeight/8.5;
            ctx.drawImage(img, pointImage.x, pointImage.y, canvasWidth/1.3, (canvasWidth/1.65)/k);
          }
          else{
            pointImage.x = canvasWidth/3;
            pointImage.y = 0;
            ctx.drawImage(img, pointImage.x, pointImage.y, canvasHeight/1.3, (canvasHeight/1.65)/k);
          }
        }else{
          if(canvasWidth < canvasHeight){
            pointImage.x = canvasWidth/12.5;
            pointImage.y = canvasHeight/3;
            ctx.drawImage(img, pointImage.x, pointImage.y, canvasHeight/1.65, (canvasHeight/1.65)/k);
          }
          else{
            pointImage.x = canvasWidth/5;
            pointImage.y = canvasHeight/8.75;
            ctx.drawImage(img, pointImage.x, pointImage.y, canvasWidth/1.65, (canvasWidth/1.65)/k);
          }
        }
        pointImageOriginal = angular.copy(pointImage);
      }

      function isVisiblePoint(point){
        return (pointImage.x <= point.x && (pointImage.x  + s) >= point.x) && (pointImage.y < point.y && (pointImage.y + s) >= point.y);
      }

      function tap(e) {
        if(isDragging){
          return;
        }

        var point = pos(e);
        if(!isVisiblePoint(point)){
          return;
        }

        //Pin value of position for the first time
        pointStart = angular.copy(point);
        // set the drag flag
        isDragging = true;
      }

      function drag(e) {
        if(!isDragging){
          return;
        }

        var point = pos(e);
        //Calculate distance between new pointer with old pointer
        diffDistance.x = parseInt(point.x - pointStart.x);
        diffDistance.y = parseInt(point.y - pointStart.y);

        //Update position start
        pointStart = angular.copy(point);

        //Redraw image in the canvas
        if (isDragging) {
          moveImage(diffDistance.x, diffDistance.y);
        }
      }

      function release() {
        if (isDragging) {
          var dx = 0;
          var dy = 0;
          if((pointImage.y + 10) < pointImageOriginal.y){
            //Continue slide image up
            dy = offsetY - (pointImage.y + s + 10);
            dx = pointImage.x > pointImageOriginal.x ? 10 : - 10;
            releaseThumb = true;
          }else{
            //Slide image back to center
            dy = pointImageOriginal.y - pointImage.y;
            dx = pointImageOriginal.x - pointImage.x;
          }
          move(dx, dy);
        }
      }

      function moveImage(x, y){
        //Update position of image
        pointImage.x += x;
        pointImage.y += y;
        ctx.clearRect(0, 0, canvasWidth, canvasHeight);
        var k = img.width/img.height;

        if(k<1){
          if(canvasWidth < canvasHeight){
            ctx.drawImage(img, pointImage.x, pointImage.y, canvasWidth/1.3, (canvasWidth/1.65)/k);
            }
          else{
            ctx.drawImage(img, pointImage.x, pointImage.y, canvasHeight/1.3, (canvasHeight/1.65)/k);
          }
        }else{
          if(canvasWidth < canvasHeight){
            ctx.drawImage(img, pointImage.x, pointImage.y, canvasHeight/1.65, (canvasHeight/1.65)/k);
          }else{

            ctx.drawImage(img, pointImage.x, pointImage.y, canvasWidth/1.65, (canvasWidth/1.65)/k);
          }
        }
      }

      function move(dx, dy) {
        var delta = makeEaseOut(bounce);
        var point = angular.copy(pointImage);
        animate({
          delay: 10,
          duration: 350, // 1 sec by default
          delta: delta,
          step: function(delta) {
            var stepX = dx*delta;
            var stepY = dy*delta;
            pointImage = angular.copy(point);
            moveImage(stepX, stepY);
          }
        })
      }

      function bounce(progress) {
        for(var a = 0, b = 1; 1; a += b, b /= 2) {
          if (progress >= (7 - 4 * a) / 11) {
            return -Math.pow((11 - 6 * a - 11 * progress) / 4, 2) + Math.pow(b, 2);
          }
        }
      }

      function makeEaseOut(delta) {
        return function(progress) {
          return 1 - delta(1 - progress)
        }
      }

      function animate(opts) {

        var start = new Date;
        var animateFrame = function() {
          var timePassed = new Date - start;
          var progress = timePassed / opts.duration;

          if (progress > 1) progress = 1;

          var delta = opts.delta(progress);
          opts.step(delta);

          if (progress == 1) {
            isDragging = false;
            if(releaseThumb){
              scope.$emit("thumb-release", {message:"finished"});
              setupEvent(true);
            }
          }else{
            requestAnimationFrame(animateFrame);
          }
        };

        animateFrame();
      }

      function pos(e) {
        e.preventDefault();
        e.stopPropagation();
        var p = {x:0, y:0};

        // touch event
        if (e.targetTouches && (e.targetTouches.length >= 1)) {
          p.x = e.targetTouches[0].clientX;
          p.y = e.targetTouches[0].clientY;
        }else{
          // mouse event
          p.x = e.clientX;
          p.y = e.clientY;
        }
        return p;
      }

      setupEvent(false);

      function setupEvent(isRemove){
        var view = document.body;
        if (typeof window.ontouchstart !== 'undefined') {
          if(isRemove){
            view.removeEventListener('touchstart', tap);
            view.removeEventListener('touchmove', drag);
            view.removeEventListener('touchend', release);
          }else{
            view.addEventListener('touchstart', tap);
            view.addEventListener('touchmove', drag);
            view.addEventListener('touchend', release);
          }
        }
        if(isRemove){
          view.removeEventListener('mousedown', tap);
          view.removeEventListener('mousemove', drag);
          view.removeEventListener('mouseup', release);
        }else{
          view.addEventListener('mousedown', tap);
          view.addEventListener('mousemove', drag);
          view.addEventListener('mouseup', release);
        }

        if(isRemove) {
          window.removeEventListener("resize", onLoadImage);
        }else{
          window.addEventListener("resize", onLoadImage);
        }
      }
    }
  };
}]);

app.constant("CONSTANT", {
  LIMIT: 7
});