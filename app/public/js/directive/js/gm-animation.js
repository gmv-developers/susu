/**
 * Created by GUMI-HUNG on 4/27/16.
 */
app.directive("animation",['api','$timeout','appFactory','socket','CONSTANT','$interval', function(api, $timeout, appFactory, socket, CONSTANT, $interval)
{
    return {
        restrict: "EAC",
        templateUrl: "js/directive/views/template_1.html",
        controller: [$scope,function($scope)
        {
            var myInterval = 3500;
            var lastclass = 0;
            $scope.slides = [];
            $scope.imageNew = [];
            $scope.selection = 0;
            $scope.x = 0;
            $scope.url = appFactory.url;
            var datapicture = [];
            var rotate = false;
            var classesImage = [
                "rotateAnimImage1",
                "rotateAnimImage2",
                "rotateAnimImage3",
                "holdImage",
                "holdImage2",
                "holdImage3"
            ];
            var classStamp = [
                "classStamp0",
                "classStamp1",
                "classStamp2",
                "classStamp3"
            ];
            api
                .photo()
                .get()
                .$promise
                .then(function (data) {
                    datapicture = data.data.reverse();
                    for (var i = 0; i < CONSTANT.LIMIT; i++) {
                        var r = Math.floor((Math.random() * datapicture.length));
                        $scope.slides.push(datapicture[r]);
                        if (datapicture > CONSTANT.LIMIT) {
                            datapicture.splice(r, 1);
                        }
                    }
                    datapicture = data.data.reverse();
                })
                .finally(function () {
                    slide();
                    $interval(function(){
                        $scope.x-=2.6;
                    }, 1000/60);
                });

            var slide = function runSlide() {
                $scope.selection++;
                var l = datapicture.length-1;
                var r = Math.floor((Math.random() * 10));
                $scope.slides[$scope.selection].className = "SlideFromRight";
                if (r < 3) {
                    if(!rotate){
                        $scope.slides[$scope.selection].classImage = classesImage[r];
                        $scope.slides[$scope.selection].classStamp = classStamp[r];
                        rotate = true;
                    }else{
                        $scope.slides[$scope.selection].classImage = classesImage[5];
                        $scope.slides[$scope.selection].classStamp = classStamp[3];
                        rotate = false;
                    }
                } else if(r>=3) {
                    $scope.slides[$scope.selection].classStamp = classStamp[3];
                    if(Math.floor(r / 3) == lastclass && r <= 8){
                        lastclass = Math.floor(r/3)+1;
                    }else{
                        lastclass = Math.floor(r/3);
                    }
                    $scope.slides[$scope.selection].classImage = classesImage[2 + lastclass];
                }
                r = Math.floor((Math.random() * l));
                $scope.slides[Math.floor(($scope.selection+1)%7)] = datapicture[r];
                console.log('r:'+l);
                if($scope.selection == CONSTANT.LIMIT-1){
                    $scope.selection = -1;
                }
                $timeout(function () {
                    slide();
                }, myInterval);
            };
            socket.on("photo-to-client", function(data){
                datapicture.push(data);
                $scope.slides[($scope.selection+1)%7].className = "SlideFromRight";
                $scope.slides[($scope.selection+1)%7].classStamp = "classStamp3";
                $scope.slides[($scope.selection+1)%7].classImage = "holdImage3";
                $scope.slides[($scope.selection+1)%7] = datapicture[datapicture.length-1];
                console.log("push comming");
                $scope.imageNew = [{
                    src: data.src,
                    orientation: data.orientation,
                    className: "PushPicture1"
                }];
            });
            angular.element(document.body).dblclick(function(){
                appFactory.toggleFullScreen();
            });
        }]
    }
}]);

/*
 * Options: Object
 *  backgroundUrl:String
 * */

/*
 * I. Contruction:
 * <div id=Background>
 *     <div getPosition id=Album1>
 *         <Image>
 *     ...
 *
 * II. Notes.
 * We have id> background = background
 *         id> Album1 = alb1
 *         id> Album2 = alb2
 * album1 & Album2 is slide show image.
 *
 * III. Function Motion: Frame and Camera.
 * 1. Camera make noise motion look like we take image by real camera.
 * 2. Frame make a move camera from position A to position B.
 * 3. We take all position for next Album appearance.
 *
 * IV. How does animation 2 work
 * 1. When start get element by ID Background & Album1 & Album2
 * 2. Make run Interval Cam sleep time 0.01s.
 * 3. After Interval run 500 times ~ 5s, We'll stop Cam and run Interval frame
 * 4. Frame'll stop when it make picture going to position i want, then restart interal Cam ...
 *
 *
 * Email: kevin30591@gmail.com
 * */



//test

app.directive("animation2", ['api','$timeout','appFactory','socket', function(api, $timeout, appFactory, socket) {
    return {
        restrict: "EAC",
        templateUrl: "js/directive/views/template_2.html",
        scope:{
            options:"="
        },
        controller:['$scope', function($scope)
        {
            var datapicture = [];
            var nextpositionX = 0;
            var nextpositionY = 0;
            var camX = 0;
            var sinx = 0;
            var runTime = 0;
            var up = "+=";
            var left = "+=";
            var moveUp = "+=";
            var moveLeft = "+=";
            var positionX = 0;
            var positionY = 0;
            var isAlbum1 = true;
            var width = window.innerWidth;
            var height = window.innerHeight;
            var pPosition = 0;
            var classnewImage = ['anim2-push-new-image1'
                ,'anim2-push-new-image2'
                ,'anim2-push-new-image3'
                ,'anim2-push-new-image4'
                ,'anim2-push-new-image5'
                ,'anim2-push-new-image6'
                ,'anim2-push-new-image7'];
            var pClass = 0;
            api
                .photo()
                .get()
                .$promise
                .then(function (data) {
                    datapicture = data.data.reverse();
                })
                .finally(function () {
                    startSlideShow();
                });

            var startSlideShow = function () {
                $scope.model = [
                    {
                        animationName: "Album1",
                        images: [{
                            picture: datapicture[0],
                            classes: 'img1',
                            size: 'anim2-image-normal2'},
                            {
                                picture: datapicture[1],
                                classes: 'img2',
                                size: 'anim2-image-normal2'},
                            {
                                picture: datapicture[2],
                                classes: 'img3',
                                size: 'anim2-image-normal2'}
                        ],
                        imageNew: []
                    },
                    {
                        animationName: "Album2",
                        images: [{
                            picture: datapicture[3],
                            classes: 'img4',
                            size: 'anim2-image-normal2'},
                            {
                                picture: datapicture[4],
                                classes: 'img5',
                                size: 'anim2-image-normal2'},
                            {
                                picture: datapicture[5],
                                classes: 'img6',
                                size: 'anim2-image-normal2'}
                        ],
                        imageNew: []
                    }
                ];


                var bg = $('#myBackground');
                var around = $('#around');

                around.css({
                    width: width + "px",
                    height: height + "px"
                });

                bg.css({
                    'background-image': "url('css/background/background6.jpg')",
                    width: width*1.2 + "px",
                    height: height*1.2 + "px"
                });
                camera();
                function frame() {
                    left = '+=';
                    up = '+=';
                    $('#'+$scope.model[0].pushimage).css({
                        'opacity' : 0
                    });

                    $('#'+$scope.model[1].pushimage).css({
                        'opacity' : 0
                    });
                    around.animate({
                        'left': nextpositionX + 'px',
                        'top': nextpositionY +'px'
                    }, 1650, "easeOutCubic");
                    bg.animate({
                        'background-position-x': nextpositionX,
                        'background-position-y': nextpositionY
                    }, 1650, "easeOutCubic", function(){
                        camera($scope);
                    });
                }

                function camera() {
                    var rt = 1000 + (Math.random() * 2500);
                    positionX = parseInt((bg.css('background-position-x')).replace("px", ""));
                    positionY = parseInt((bg.css('background-position-y')).replace("px", ""));

                    if (nextpositionX > positionX && positionX - nextpositionX <= -15) {
                        left = '+=';
                    } else if (nextpositionX < positionX && positionX - nextpositionX >= 10) {
                        left = '-=';
                    }
                    if (nextpositionY > positionY && positionY - nextpositionY <= -12) {
                        up = '+=';
                    } else if (nextpositionY < positionY && positionY - nextpositionY >= 17) {
                        up = '-=';
                    }
                    camX = window.innerWidth / 1200 * (rt) / 80;
                    sinx = Math.random()*0.6;

                    around.animate({
                        'left': left + camX,
                        'top' : up + camX*0.35
                    }, rt/1.5, "linear");

                    bg.animate({
                        'background-position-x': left + camX ,
                        'background-position-y': up + camX*0.35
                    }, rt/1.5, "linear", function () {
                        checkingTime();
                    });
                }


                function checkingTime(){
                    runTime++;
                    if(runTime>=6){
                        resetNewImage();
                        if(Math.floor(Math.random()*2)==0){
                            moveUp = 1
                        }else{
                            moveUp = -1
                        }
                        if(Math.floor(Math.random()*2)==0){
                            moveLeft = 1
                        }else{
                            moveLeft = -1
                        }
                        nextpositionX += (width + Math.random()*width)*moveLeft;
                        nextpositionY += (height+ Math.random()*height)*moveUp;
                        if(isAlbum1){
                            $('#Album2').css({
                                'left' : -1*nextpositionX + width/4 +'px',
                                'top' : -1*nextpositionY +height/4 +'px'
                            });
                            isAlbum1 = false;
                        }else{
                            $('#Album1').css({
                                'left' : -1*nextpositionX + width/4+'px',
                                'top' : -1*nextpositionY + height/4+'px'
                            });
                            isAlbum1 = true;
                        }
                        setclass();
                        runTime = 0;
                        frame();
                    }else{
                        camera();
                    }}

                function setclass() {
                    var isTop = 1;
                    if(Math.floor(Math.random()*2)>0){
                        isTop = -1;
                    }
                    var csspositionleft = [width/9 + width / (4 + Math.floor((Math.random() * 5)))
                        , width/9
                        , width /9 + -width / (4 + Math.floor((Math.random() * 5)))];
                    var csspositiontop = [isTop*height / (4 + Math.floor((Math.random() * 5)))
                        , 0
                        , isTop*-height / (4 + Math.floor((Math.random() * 5)))];
                    var cssrotate = [
                        , 5 + Math.floor((Math.random() * 5))
                        , -5 + -(Math.floor((Math.random() * 5)))
                        , -10 + (Math.floor((Math.random() * 20)))];
                    var numberal = 1;
                    if (!isAlbum1) {
                        numberal = 1;
                    } else {
                        numberal = 0;
                    }
                    for (var i = 0; i < 3; i++) {
                        var img = $scope.model[numberal].images[i];
                        img.picture = datapicture[pPosition];

                        $('#' + img.classes).css({
                            'left': csspositionleft[i] + 'px',
                            'top': csspositiontop[i] + 'px',
                            '-moz-transform': 'rotate(' + cssrotate[i] + 'deg)',
                            '-webkit-transform': 'rotate(' + cssrotate[i] + 'deg)',
                            'transform': 'rotate(' + cssrotate[i] + 'deg)'
                        });
                        pPosition++;
                        if (pPosition > datapicture.length - 1) {
                            pPosition = 0;
                        }
                    }
                    if(!$scope.$$phase){
                        $scope.$apply();
                    }
                }
                function resetNewImage(){
                    $scope.model[0].imageNew= [];
                    $scope.model[1].imageNew = [];
                }
            };
            socket.on("photo-to-client", function(data){
                console.log('push image comming');
                runTime += -3;
                if(runTime<-1){
                    runTime =-1;
                }
                datapicture.push(data);
                if(isAlbum1){
                    $scope.model[0].imageNew.push({
                        src: data.src,
                        orientation: data.orientation,
                        className: classnewImage[pClass]});
                }else{
                    $scope.model[1].imageNew.push({
                        src: data.src,
                        orientation: data.orientation,
                        className: classnewImage[pClass]});
                }
                pClass++;
                if(pClass>classnewImage.length-1){
                    pClass =0;
                }
                if(!$scope.$$phase){
                    $scope.$apply();
                }
            });
            angular.element(document.body).dblclick(function(){
                appFactory.toggleFullScreen();
            });
        }]
    }
}]);


