/**
 * Created by GUMI-NGHI on 4/27/16.
 */
app.directive("animation3",['api','socket', function(api, socket) {

        return {
            restrict: "EAC",
            templateUrl: "js/directive/views/template_3.html",
            controller: function ($scope) {

/*                //Set full page for canvas
                //1. Old canvas: to display old images user posted
                var oldCanvas = document.getElementById('oldCanvas');
                var ctx = oldCanvas.getContext('2d');
                oldCanvas.width = window.innerWidth;
                oldCanvas.height = window.innerHeight;

                //2. New canvas: to display new images users have just posted
                var newCanvas = document.getElementById('newCanvas');
                newCanvas.width = window.innerWidth;
                newCanvas.height = window.innerHeight;

                //3. Intro canvas: to display content before animation
                var introCanvas = document.getElementById('introCanvas');
                introCanvas.width = window.innerWidth;
                introCanvas.height = window.innerHeight;*/

                //4. Ending canvas
                var endCanvas = document.getElementById('endCanvas');
                endCanvas.width = window.innerWidth;
                endCanvas.height = window.innerHeight;

/*
                //Create object for each
                var oldCanvasObj = oCanvas.create({
                    canvas: oldCanvas
                });
                var newCanvasObj = oCanvas.create({
                    canvas: newCanvas
                });
                var introCanvasObj = oCanvas.create({
                    canvas: introCanvas
                });*/
                var endCanvasObj = oCanvas.create({
                    canvas: endCanvas
                });


                // //Register callback to show intro screen
                // oldCanvas.addEventListener("webkitAnimationEnd", showIntroContent,false);
                // oldCanvas.addEventListener("animationend", showIntroContent,false);
                // oldCanvas.addEventListener("oanimationend", showIntroContent,false);
                //
                //
                ////Parameters for old canvas
                // var images = ["js/directive/availUploads/image1.jpg",
                // "js/directive/availUploads/image2.jpg",
                // "js/directive/availUploads/image3.jpg",
                // "js/directive/availUploads/image4.jpg",
                // "js/directive/availUploads/image5.jpg"];
                // var nColumnScreen = 6;
                // var nRowScreen = 5; //min : 4
                // var unitWidth  = oldCanvas.width / nColumnScreen;
                // var unitHeight = oldCanvas.height / nRowScreen;
                // var totalImage = nColumnScreen * nRowScreen;
                // var IMGSize = unitWidth;
                //
                //
                // var minNumOfImgToCreateFirstBuffer = 5;
                // var chooseList = [].fill.call({length: totalImage * 3}, 0);
                // var trackImage = [-1, -1, -1];
                // var trackSource = [0, 0, 0];
                // var buffers = [0, 0, 0];
                // var iBuf = 0;
                // var fMoveLeft = false;
                // var interval = 30;//interval between two times of moving
                // var moveDist = 2;//offset distance
                // var mL = moveDist; //Margin left to start drawing first buffer
                //
                //
                // // Parameters for new image
                // var NEWSize = newCanvas.width / 2;
                // var centerTargetX = newCanvas.width/2;
                // var centerTargetY = newCanvas.height/2;
                //
                ///*********  Functions  **************/
                //
                // //get old images
                // api
                // .photo()
                // .get()
                // .$promise
                // .then(function (data) {
                // for(var i = 0 ; i < data.data.length; i++){
                // images.push(data.data[i].src);
                // }
                // });
                //
                // //interact with client
                // socket.on("photo-to-client", function(data){
                // if(buffers[0]!==0) createNewImage(data.src);
                // images.push(data.src);
                // });
                //
                //
                // //1. Show intro screen
                // function randomInteger(low, high){
                // return low + Math.floor(Math.random() * (high - low));
                // }
                // function showIntroContent() {
                // //Keep screen show after animation
                // introCanvas.style.opacity=1;
                // oldCanvas.style.opacity=1;
                //
                // setTimeout(function () {
                //
                // //Start snow fall
                // startSnowFall();
                //
                // //Animate content
                // animateIntroContent();
                //
                // },500);
                // }
                // function animateIntroContent() {
                //
                // var delay = 2000;
                // var duration = 2000;
                //
                // //-------------------------
                // var text1 = introCanvasObj.display.text({
                // x: introCanvas.width/2,
                // origin: {x: "center", y: "center"},
                // font: "60px BlackChen",
                // text: "Have A Happy Wedding",
                // fill: "rgba(0,0,0,0.5)",
                // shadow: "0 0 10px #000",
                // opacity: 0
                // });
                // text1.y = introCanvas.height/3 + text1.height;
                // introCanvasObj.addChild(text1);
                // text1.delay(delay);
                // text1.animate({
                // opacity: 1,
                // y: introCanvas.height/3
                // },{
                // duration: duration,
                // easing: "ease-out-quad"
                // });
                //
                // //-------------------------
                // var text2 = introCanvasObj.display.text({
                // x: introCanvas.width/2,
                // y: 0,
                // origin: {x: "center", y: "center"},
                // font: "20px",
                // text: "14 - 2 - 2016",
                // stroke: "2px #f0f",
                // fill: "#fff",
                // shadow: "0 0 20px #f0f"
                // });
                // var line1 = introCanvasObj.display.line({
                // start: {x: introCanvas.width/2 - text2.width/2 - 100, y: 0},
                // end: {x: introCanvas.width/2 - text2.width/2 - 20, y: 0},
                // stroke: "3px rgba(0,0,0,0.5)",
                // cap: "round"
                // });
                // var line2 = introCanvasObj.display.line({
                // start: {x: introCanvas.width/2 + text2.width/2 + 100, y:0},
                // end: {x: introCanvas.width/2 + text2.width/2 + 20, y:0},
                // stroke: "3px rgba(0,0,0,0.5)",
                // cap: "round"
                // });
                // var frame = introCanvasObj.display.rectangle({
                // x:0,
                // y:introCanvas.height / 2 + 100 - text1.height,
                // width: introCanvas.width,
                // height: text2.height,
                // opacity:0
                // });
                // frame.addChild(text2);
                // frame.addChild(line1);
                // frame.addChild(line2);
                // introCanvasObj.addChild(frame);
                //
                // frame.delay(delay);
                // frame.animate({
                // opacity: 1,
                // y: introCanvas.height / 2 + 100
                // },{
                // duration: duration,
                // easing: "ease-out-quad"
                // });
                //
                // //-------------------------
                // var heart = introCanvasObj.display.image({
                // x: introCanvas.width/2,
                // y: introCanvas.height/2,
                // origin: {x: "center", y: "center"},
                // image: "../../../img/anim3_hearts.png",
                // width: 1.5,
                // height: 1.25
                // });
                // introCanvasObj.addChild(heart);
                // heart.delay(delay);
                // heart.animate({
                // rotation: 360,
                // width: 150,
                // height: 125
                // },{
                // duration: duration,
                // easing: "ease-out-quad",
                // callback: function () {
                // setTimeout(function () {
                // text1.fadeOut();
                // frame.fadeOut();
                // heart.fadeOut("normal", "ease-in-out-cubic", function () {
                // layoutFirstScreen();
                // });
                // },1000);
                // }
                // });
                // }
                //
                // //2. Show 5 first images
                // var iIntroImg  = 0;
                // var nIntroImg  = 10;
                // function layoutFirstScreen(){
                //
                // //Set x
                // var x = (iIntroImg < 6)
                // ? (oldCanvas.width/6 + (oldCanvas.width/9*2)  * (iIntroImg%3+0.5))
                // : (oldCanvas.width/8 + (oldCanvas.width/16*3) * (iIntroImg-5.5));
                //
                // //Set y and add more random distance
                // var y = iIntroImg < 3 ? oldCanvas.height/4
                // : (iIntroImg < 6 ? oldCanvas.height*0.75 : oldCanvas.height/2);
                //
                //
                // //Create image
                // var newImg = {"x":x,"y":y};
                // var img  = new Image();
                // img.crossOrigin = '';
                // img.src  = images[iIntroImg % 5];
                // img.index  = iIntroImg++;
                // newImg.obj = img;
                //
                // //Rotate angle
                // newImg.rotateAngle = Math.round(Math.random()*60)-30;
                //
                // //Scale factor
                // newImg.scaleFactor  = Math.random()*0.2 + 0.6;
                //
                // //Add to list
                // chooseList[img.index]=newImg;
                //
                // //Load image finish
                // img.onload  = layoutFirstCallback;
                // img.onerror = layoutFirstCallback;
                //
                // }
                // function layoutFirstCallback(){
                //
                // var newImg  = chooseList[this.index];
                //
                // //Adjust size
                // newImg.originWidth  = this.width;
                // newImg.originHeight = this.height;
                // if(newImg.originWidth >= newImg.originHeight){
                // newImg.scaleWidth  = newImg.scaleFactor * IMGSize;
                // newImg.scaleHeight = newImg.originHeight/(newImg.originWidth/newImg.scaleWidth);
                // }
                // else{
                // newImg.scaleHeight = newImg.scaleFactor * IMGSize;
                // newImg.scaleWidth  = newImg.originWidth/(newImg.originHeight/newImg.scaleHeight);
                // }
                //
                // //Create next image
                // if(this.index==nIntroImg-1) { //finish creation
                // animateIntroImages();
                // }
                // else{ //create next image
                // layoutFirstScreen();
                // }
                // }
                // function animateIntroImages() {
                // for(var i = 0; i < nIntroImg; i++){
                // var image  = chooseList[i];
                // var imgObj = oldCanvasObj.display.image({
                // x: oldCanvas.width/2,
                // y: oldCanvas.height/2,
                // width: 1,
                // height: 1,
                // origin: {x: "center", y: "center"},
                // image: image.obj,
                // stroke: "0 #fff",
                // shadow: "0 0 0px #000",
                // opacity: 1
                // });
                // oldCanvasObj.addChild(imgObj);
                // imgObj.delay(1000);
                // imgObj.animate({
                // index: i,
                // x: image.x,
                // y: image.y,
                // width: image.scaleWidth,
                // height: image.scaleHeight,
                // rotation: image.rotateAngle,
                // strokeWidth: 5,
                // shadowBlur: 30
                // },{
                // duration: 2000,
                // easing: "ease-out-quad",
                // callback: function () {
                //
                // if(imgObj.index == nIntroImg-1) {//only call when last image created
                //
                // introCanvasObj.reset();
                //
                // //Restore first buffer
                // buffers[0] = document.createElement('canvas');
                // buffers[0].width = oldCanvas.width;
                // buffers[0].height = oldCanvas.height;
                // var bufCXT = buffers[0].getContext('2d');
                // bufCXT.drawImage(oldCanvas, 0, 0);
                //
                // //Create second buffer
                // setTimeout(function(){
                // chooseImageToRepeat(1);
                // },1000);
                // }
                // }
                // });
                // }
                // }
                //
                //
                // //3. Prepare buffer and draw posted images
                // function drawScreen() {
                //
                // //when to create next buffer
                // if (mL <= 0 && mL > -moveDist) {
                // chooseImageToRepeat((iBuf + 2) % 3);
                // }
                // else if (mL <= -oldCanvas.width && mL > (-oldCanvas.width - moveDist)) {
                // iBuf = (iBuf + 1) % 3;
                // chooseImageToRepeat((iBuf + 2) % 3);
                // mL += oldCanvas.width;
                // }
                //
                // //draw screen
                // ctx.save();
                // ctx.clearRect(0, 0, oldCanvas.width, oldCanvas.height);
                //
                // ctx.drawImage(buffers[iBuf], mL, 0, oldCanvas.width, oldCanvas.height);
                // ctx.drawImage(buffers[(iBuf + 1) % 3], mL + oldCanvas.width, 0, oldCanvas.width, oldCanvas.height);
                // //ctx.drawImage(clippedBuffers[iBuf], mL + oldCanvas.width/2, 0, clippedBuffers[iBuf].width, clippedBuffers[iBuf].height);
                //
                // ctx.restore();
                // }
                // function chooseImageToRepeat(iBuffer){
                //
                // //Find next index of image
                // var iSrc = trackSource[iBuffer];
                // var iImg = trackImage[iBuffer];
                // if(iImg==-1){
                // iImg=0;
                // iSrc= randomInteger(images.length-minNumOfImgToCreateFirstBuffer,images.length- 1);
                // }
                // else{
                // iImg++;
                // iSrc=(iSrc - randomInteger(1,5) + images.length)%images.length;
                // }
                // trackImage[iBuffer]  = iImg;
                // trackSource[iBuffer] = iSrc;
                //
                //
                // //Determine column and row index
                // var c = Math.floor(iImg/nRowScreen);
                // var r = iImg % nRowScreen;
                //
                //
                // //Calculate x & y
                // var x = unitWidth * c;
                // var y = unitHeight * r ;
                // if(r <= 1 || r == nRowScreen - 1){
                // y += IMGSize/8;
                // }
                // else {
                // y += Math.floor(Math.random()*IMGSize/2)-IMGSize/4;
                // }
                //
                //
                // //Create image
                // var newImg = {"x": x ,"y": y };
                // var img  = new Image();
                // img.crossOrigin = '';
                // img.iBuf = iBuffer;
                // img.src  = images[iSrc];
                // img.index  = iBuffer*totalImage + iImg;
                // newImg.obj = img;
                // chooseList[img.index]=newImg;
                //
                //
                // //Rotate angle
                // newImg.rotateAngle = Math.round(Math.random()*90)-45;
                //
                //
                // //Scale factor
                // newImg.scaleFactor  = (r==0||r== nRowScreen-1) ? 0.8 : ((r==1 || r==nRowScreen-2) ? 0.7 : 0.5); //Blur factor
                // newImg.blurType = (r==0|| r == nRowScreen-1) ? "" : ((r==1 || r==nRowScreen-2) ? Filters.GAUSSIAN : Filters.BOX2); //Add to list
                //
                //
                // //Load image finish
                // img.onload  = chooseImageToRepeatCallBack;
                // img.onerror = chooseImageToRepeatCallBack;
                // }
                // function chooseImageToRepeatCallBack(){
                // var newImg  = chooseList[this.index];
                // var iBuffer = this.iBuf;
                //
                // //Determine size
                // newImg.originWidth  = this.width;
                // newImg.originHeight = this.height;
                // if(newImg.originWidth >= newImg.originHeight){
                // newImg.scaleWidth  = newImg.scaleFactor * IMGSize;
                // newImg.scaleHeight = newImg.originHeight/(newImg.originWidth/newImg.scaleWidth);
                // }
                // else{
                // newImg.scaleHeight = newImg.scaleFactor * IMGSize;
                // newImg.scaleWidth  = newImg.originWidth/(newImg.originHeight/newImg.scaleHeight);
                // }
                //
                // //Adjust x
                // var offset = Math.sqrt(newImg.scaleWidth*newImg.scaleWidth + newImg.scaleHeight*newImg.scaleHeight)/2;
                // if(newImg.x == 0){ //first column
                // newImg.x = offset -  newImg.scaleWidth/2;
                // if(newImg.y > 0 && newImg.y < oldCanvas.height-unitHeight){
                // newImg.x += Math.random()*unitWidth/4;
                // }
                // }
                // else if(newImg.x == oldCanvas.width-unitWidth){ //last column
                // newImg.x = oldCanvas.width - 2*offset ;
                // if(newImg.y > 0 && newImg.y < oldCanvas.height-unitHeight){
                // newImg.x -= Math.random()*unitWidth/4;
                // }
                // }
                // else{ //remain columns
                // newImg.x += (unitWidth-newImg.scaleWidth)/2;
                // if(newImg.y > 0 && newImg.y < oldCanvas.height-unitHeight){
                // newImg.x += Math.random()*unitWidth - unitWidth/2;
                // }
                // }
                //
                // //Finish creation
                // if(this.index==((iBuffer+1)* totalImage-1)) {
                //
                // trackImage[iBuffer]  = -1;
                // trackSource[iBuffer] = 0;
                //
                // createBuffer(iBuffer);
                //
                // if(!fMoveLeft){
                // fMoveLeft=true;
                // setInterval(function(){
                // mL-=moveDist;
                // drawScreen();
                // },interval);
                // }
                // }
                // else{ //create next image
                // chooseImageToRepeat(iBuffer);
                // }
                // }
                // function createBuffer(iBuffer){
                //
                // var buf    = document.createElement('canvas');
                // buf.width  = oldCanvas.width;
                // buf.height = oldCanvas.height;
                // var bufCXT = buf.getContext('2d');
                //
                // var iImg = iBuffer*totalImage;
                // for(var i=0; i<nColumnScreen; i++){
                // for(var j=2; j < nRowScreen-2; j++){
                // drawPhoto(chooseList[iImg+j], bufCXT);
                // }
                // drawPhoto(chooseList[iImg + 1], bufCXT);
                // drawPhoto(chooseList[iImg + nRowScreen-2] , bufCXT);
                // drawPhoto(chooseList[iImg], bufCXT);
                // drawPhoto(chooseList[iImg + nRowScreen-1], bufCXT);
                // iImg += nRowScreen;
                // }
                // buffers[iBuffer]= buf;
                // }
                // function drawPhoto(newImg, bufCXT) {
                //
                // if(newImg.blurType=="") {
                //
                // bufCXT.translate(newImg.x, newImg.y);
                //
                // bufCXT.translate(newImg.scaleWidth/2, newImg.scaleHeight/2);
                // bufCXT.rotate(newImg.rotateAngle * Math.PI/180);
                //
                //
                // setShadow(bufCXT,8);
                // bufCXT.fillStyle = "#FFFFFF";
                // bufCXT.fillRect(-newImg.scaleWidth/2, -newImg.scaleHeight/2, newImg.scaleWidth, newImg.scaleHeight);
                //
                //
                // noShadow(bufCXT);
                // bufCXT.drawImage(newImg.obj, 0, 0, newImg.originWidth, newImg.originHeight, -newImg.scaleWidth/2 + 5, -newImg.scaleHeight/2 + 5, newImg.scaleWidth - 10, newImg.scaleHeight - 10);
                //
                //
                // bufCXT.rotate(-newImg.rotateAngle * Math.PI/180);
                // bufCXT.translate(-newImg.scaleWidth/2, -newImg.scaleHeight/2);
                // bufCXT.translate(-newImg.x, -newImg.y);
                // }
                // else{
                //
                // //draw to temp canvas
                // var buf  = document.createElement('canvas');
                // buf.width  = newImg.scaleWidth;
                // buf.height = newImg.scaleHeight;
                // var cxt = buf.getContext('2d');
                //
                // cxt.fillStyle = "#FFFFFF";
                //
                // cxt.fillRect(0,0,buf.width,buf.height);
                // cxt.drawImage(newImg.obj,0,0, newImg.originWidth, newImg.originHeight,5,5,buf.width-10, buf.height- 10); //blur image
                // var output = Filters.filterImage(Filters.convolute,buf,newImg.blurType);
                // cxt.putImageData(output,0, 0);
                //
                //
                // //draw to canvas
                // bufCXT.translate(newImg.x, newImg.y);
                // bufCXT.translate(newImg.scaleWidth/2, newImg.scaleHeight/2);
                // bufCXT.rotate(newImg.rotateAngle * Math.PI/ 180);
                //
                // bufCXT.drawImage(buf,-newImg.scaleWidth/2, -newImg.scaleHeight/2);
                //
                // bufCXT.rotate(-newImg.rotateAngle * Math.PI/180);
                // bufCXT.translate(-newImg.scaleWidth/2, -newImg.scaleHeight/2);
                // bufCXT.translate(-newImg.x, -newImg.y);
                // }
                // }
                // function setShadow(bufCXT, offset){
                // bufCXT.shadowBlur =20;
                // bufCXT.shadowColor ="#000";
                // bufCXT.shadowOffsetX = offset;
                // bufCXT.shadowOffsetY = offset;
                // }
                // function noShadow(bufCXT){
                // bufCXT.shadowBlur =0;
                // bufCXT.shadowOffsetX = 0;
                // bufCXT.shadowOffsetY = 0;
                // }
                //
                // //4. Draw new image
                // function createNewImage(imgSrc){
                //
                // var img = new Image();
                // img.onload = function() {
                //
                // var newImg = {};
                //
                // //Rotation angle
                // newImg.rotateAngle = Math.round(Math.random() * 60) - 30;
                //
                // //Origin size
                // newImg.originWidth  = this.width;
                // newImg.originHeight = this.height;
                //
                // //Adjust size
                // if (newImg.originWidth >= newImg.originHeight) {
                // newImg.scaleWidth = NEWSize;
                // newImg.scaleHeight = newImg.originHeight / (newImg.originWidth / NEWSize);
                // }
                // else {
                // newImg.scaleHeight = NEWSize;
                // newImg.scaleWidth = newImg.originWidth / (newImg.originHeight / NEWSize);
                // }
                //
                //
                // //Set start position
                // var startLocation = randomInteger(0, 5);
                // if (startLocation == 0) { //from left-bottom
                // newImg.y = newCanvas.height + newImg.scaleHeight / 2;
                // newImg.x = newCanvas.width / 4;
                // }
                // else if (startLocation == 1) {//from right-bottom
                // newImg.y = newCanvas.height - newImg.scaleHeight / 2;
                // newImg.x = newCanvas.width * 5 / 4;
                // }
                // else if (startLocation == 2){ //from right-top
                // newImg.y = 0;
                // newImg.x = newCanvas.width * 5 / 4;
                // }
                //
                //
                // //Add new photo to canvas
                // var photo = newCanvasObj.display.image({
                // x: newImg.x,
                // y: newImg.y,
                // origin: {x: "center", y: "center"},
                // width:   newImg.scaleWidth,
                // height:  newImg.scaleHeight,
                // image: this,
                // stroke : "15 #fff",
                // shadow: "0 0 20px #000",
                // rotation: newImg.rotateAngle,
                // opacity: 0
                // });
                // newCanvasObj.addChild(photo);
                //
                // //Animation new photo
                // photo.animate({
                // x: centerTargetX,
                // y: centerTargetY,
                // opacity: 1
                // }, {
                // duration: 3500,
                // easing: "ease-out-quint"
                // });
                //
                // var targetX = Math.floor(Math.random() * newCanvas.width/4) + newCanvas.width/2;
                // var targetY = Math.floor(Math.random() * 100) - 50 + centerTargetY;
                //
                // photo.animate({
                // x: targetX,
                // y: targetY,
                // width: photo.width/3,
                // height: photo.height/3,
                // strokeWidth: "5",
                // rotation: photo.rotation + 360
                // }, {
                // duration: 2000,
                // easing: "ease-in-out-quad",
                // callback: function () {
                //
                // //remove image from newCanvas
                // setTimeout(function () {
                // newCanvasObj.removeChild(photo);
                // }, interval);
                //
                // //draw a part of image on before buffer of oldCanvas
                // if((photo.x - photo.width/2) < (oldCanvas.width + mL)) {
                // drawNewOnBuffer(photo,buffers[iBuf].getContext('2d'), -mL);
                // }
                // //draw a part of image on after buffer of oldCanvas
                // if((photo.x + photo.width/2) > oldCanvas.width + mL){
                // drawNewOnBuffer(photo,buffers[(iBuf+1)%3].getContext('2d'),- oldCanvas.width - mL);
                // }
                // }
                // });
                // }
                // img.src = imgSrc;
                // }
                // function drawNewOnBuffer(photo,bufCXT,offset) {
                //
                // bufCXT.translate(photo.x - photo.width/2 + offset, photo.y - photo.height / 2);
                // bufCXT.translate(photo.width / 2, photo.height / 2);
                // bufCXT.rotate(photo.rotation * Math.PI / 180);
                //
                // bufCXT.fillStyle = "#FFFFFF";
                // setShadow(bufCXT, 0);
                // bufCXT.fillRect(-photo.width / 2, -photo.height / 2, photo.width, photo.height);
                // noShadow(bufCXT);
                // bufCXT.drawImage(photo.image, 0, 0, photo.image.width, photo.image.height,
                // -photo.width / 2 + 5, -photo.height / 2 + 5, photo.width - 10, photo.height - 10);
                //
                //
                // bufCXT.rotate(-photo.rotation * Math.PI / 180);
                // bufCXT.translate(-photo.width / 2, -photo.height / 2);
                // bufCXT.translate(-(photo.x - photo.width / 2 + offset), -(photo.y - photo.height / 2));
                // }

                 //5. Ending
                 var images = ["js/directive/availUploads/image1.jpg",
                             "js/directive/availUploads/image2.jpg",
                             "js/directive/availUploads/image3.jpg",
                             "js/directive/availUploads/image4.jpg",
                             "js/directive/availUploads/image5.jpg"];
                 function randomInteger(low, high){
                    return low + Math.floor(Math.random() * (high - low));
                 }

                 var heartFrameSize = endCanvas.height;
                 var imageSize = 80;
                 var endAreas  = [{
                     left: (endCanvas.width-heartFrameSize)/2 + heartFrameSize/5,
                     top: 0,
                     width: heartFrameSize/5*4-imageSize/2,
                     height: heartFrameSize/3*2
                 },
                 {
                     left: (endCanvas.width-heartFrameSize)/2+imageSize/2,
                     top: heartFrameSize/3*2,
                     width: heartFrameSize*3/5,
                     height: heartFrameSize/3
                 }];
                 for(var i = 0; i < endAreas.length; i++){
                     endAreas[i].numImg = Math.floor(endAreas[i].width/imageSize);
                     endAreas[i].numImg = endAreas[i].numImg * endAreas[i].numImg;
                 }
                 addImageToEndingFrame(0,0,endAreas[0].left,endAreas[0].top);


                 function addImageToEndingFrame(iImg,iArea,left,top){

                    var image  = new Image();
                    image.iArea  = iArea;
                    image.index  = iImg;
                    image.src    = images[image.index % images.length];
                    image.left   = left;
                    image.top    = top;
                    image.onload = function () {

                        var img = {obj:this};

                        //Adjust size
                        if (this.width >= this.height) {
                            img.width = imageSize;
                            img.height = this.height / (this.width / imageSize);
                        }
                        else {
                            img.height = imageSize;
                            img.width = this.width / (this.height / imageSize);
                        }

                        //Rotation angle
                        img.rotateAngle = Math.round(Math.random() * 120) - 60;

                        //Draw image on canvas
                        var endingImage = endCanvasObj.display.image({
                            x: img.obj.left + imageSize/2,
                            y: img.obj.top,
                            origin: {x: "center", y: "center"},
                            rotation: img.rotateAngle,
                            stroke : "2 #fff",
                            shadow: "0 0 5px #000",
                            image: img.obj,
                            height: img.height,
                            width: img.width,
                            scalingX: 0.9,
                            scalingY: 0.9
                        });
                        endCanvasObj.addChild(endingImage);

                        //Choose next
                        chooseNextImageForEndingFrame(img.obj.index+1,img.obj.iArea,img.obj.left,img.obj.top);
                    }
                }
                 function chooseNextImageForEndingFrame(iImg,iArea,left,top){

                    if(iImg == endAreas[iArea].numImg && (iArea+1)==endAreas.length){
                        var heartFrame = endCanvasObj.display.image({
                            x: endCanvas.width/2,
                            y: 0,
                            origin: {x: "center"},
                            image: "../../../img/ending_frame.png",
                            height: endCanvas.height,
                            width: endCanvas.height
                        });
                        endCanvasObj.addChild(heartFrame);
                        return;
                    }

                    var nextArea  = iImg < endAreas[iArea].numImg ? iArea : (iArea+1);
                    var nextIndex = iImg < endAreas[iArea].numImg ? iImg : 0;

                    if(iImg < endAreas[iArea].numImg){
                        left += imageSize;
                        if(left + imageSize >= endAreas[nextArea].left + endAreas[nextArea].width) {
                            left = endAreas[nextArea].left;
                            top  += imageSize;
                            if(top >= endAreas[nextArea].top + endAreas[nextArea].height) {
                                top = endAreas[nextArea].top;
                            }
                        }
                    }
                    else{
                        left = endAreas[nextArea].left;
                        top  = endAreas[nextArea].top;
                    }
                    addImageToEndingFrame(nextIndex,nextArea,left,top);
                }
            }
        }
}]);


