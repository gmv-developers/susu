var express = require('express');
var app = express();
var server = require('http').Server(app);
var cors = require('cors');
var multer  = require('multer');
var port = process.env.PORT || 8082;
var jsonfile = require('jsonfile');
var util = require('util');
var file = './tmp/data.json';
var images = jsonfile.readFileSync(file);
var ip = require("./api/ip");
var lwip = require('lwip');
var fs = require('fs');


var storage =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './uploads');
  },
  filename: function (req, file, callback) {
    var re = /(?:\.([^.]+))?$/;
    var ext = re.exec(file.originalname)[0];
    callback(null, Date.now() + ext);
  }
});

var upload = multer({
  storage : storage
}).single('file');

app.use(cors());

//app.use(function(req, res, next){
//  console.log(req.method + "-" + req.url);
//  next();
//});

app.use("/uploads", express.static('uploads'));
app.post('/api/photo',function(req,res){
  upload(req,res,function(err) {
    if(err) {
      return res.json({
        message:"Error uploading file."
      });
    }

    fs.readFile(req.file.path, function (err, data) {
      if (err) throw err;
      var ext = req.file.originalname.split(".");
      lwip.open(data, ext[1], function(err, image){
        if(err) throw err;
          switch(+ext[0]) {
              case 2:
                  image = image.batch().flip('x'); // top-right - flip horizontal
                  break;
              case 3:
                  image = image.batch().rotate(180); // bottom-right - rotate 180
                  break;
              case 4:
                  image = image.batch().flip('y'); // bottom-left - flip vertically
                  break;
              case 5:
                  image = image.batch().rotate(90).flip('x'); // left-top - rotate 90 and flip horizontal
                  break;
              case 6:
                  image = image.batch().rotate(90); // right-top - rotate 90
                  break;
              case 7:
                  image = image.batch().rotate(270).flip('x'); // right-bottom - rotate 270 and flip horizontal
                  break;
              case 8:
                  image = image.batch().rotate(270); // left-bottom - rotate 270
                  break;
              default:
                  image = image.batch();
                  break;
          }
        // image can now be used as per normal with batch
        // eg. image.resize(200, 200)....
        image.writeFile(req.file.path, function (error) {
          if(error) {
            return res.json({
              message:"Error uploading file."
            });
          }else{
            res.json({
              message: "File is uploaded",
              path: req.file.path,
              filename: req.file.filename
            });
          }
        })
      });
    });

  });
});

app.get('/api/photo',function(req,res){
  res.json({
    data:images
  })
});


app.use(express.static('public'));
app.use(function(req, res) {
  res.sendFile(__dirname + '/public/index.html');
});

server.listen(port, function(){
  ip.getNetworkIPs(function(error, ip){
    console.log("Server started at port " + ip + ":" + port);
  });
});


var io = require('socket.io')(server);
io.on('connection', function (socket) {
  //Update image data for new pc connection
  socket.emit('fetch-data', images[images.length - 1]);

  //Receive data from sp client
  socket.on('photo-to-server', function (data) {
    images.push({
      src: "/" + data.path
    });
    jsonfile.writeFileSync(file, images);
    //Send data to pc client
    socket.broadcast.emit('photo-to-client', {
      src: "/" + data.path
    });
  });
});

// lwip.open(req.file.path,function(err,image){
//   var width = Number(image.width());
//   var height = Number(image.height());
//   var k = height/1080;
//   if(height>width){
//     k = width/1080;
//   }
//   fs.readFile(req.file.path, function (err, data){
//     var exifData = false;
//     exifData = exif.create(data).parse();
//     // image.resize(width/k,height/k,"lanczos",function(err,img){
//     //   console.log(img.size());
//     //   console.log(exifData.tags.Orientation);
//     //   if(exifData.tags.Orientation == 6) {
//     //     img = img.rotate(90);
//     //   }
//
//     res.json({
//       message:"File is uploaded",
//       path: req.file.path,
//       filename: req.file.filename
//     });
//   });
// });
// });
// });




